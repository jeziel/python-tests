FROM python:alpine

RUN mkdir /app /test

WORKDIR /app
VOLUME /app

ENTRYPOINT ["/app/scripts/entrypoint.sh"]
