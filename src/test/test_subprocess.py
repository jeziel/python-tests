
import sys
import subprocess
import unittest


"""
From: https://www.digitalocean.com/community/tutorials/how-to-use-subprocess-to-run-external-programs-in-python-3
"""
class SubprocessTests(unittest.TestCase):
    script_home = '/app/main/scripts/{}'

    def test_subprocess_executes_a_command(self):
        args = ['ls', '-1']

        result = subprocess.run(args)

        self.assertIsNotNone(result)
        self.assertEqual(args, result.args)
        self.assertIsNone(result.stdout, 'not capturing output')
        self.assertIsNone(result.stderr, 'not capturing err')
        self._validate_not_failed(result)

    def test_subprocess_executes_a_failed_command(self):
        args = [self.script_home.format('command_error.sh')]
        result = subprocess.run(args)

        self.assertIsNotNone(result)
        self.assertEqual(args, result.args)
        self.assertEqual(1, result.returncode)
        self.assertIsNone(result.stdout, 'not capturing output')
        self.assertIsNone(result.stderr, 'not capturing err')
        self.assertRaises(subprocess.CalledProcessError, result.check_returncode)

    def test_subprocess_raises_exception_when_timeout(self):
        args = [self.script_home.format('command_timeout.sh')]
        result = None

        try:
            result = subprocess.run(args, timeout=0.5)
            self.fail('A process that times out should raise an exception')
        except subprocess.TimeoutExpired as e:
            self.assertIsNone(result)

    def test_subprocess_can_capture_the_stdout(self):
        args = [self.script_home.format('command_stdout.sh')]
        result = subprocess.run(args, capture_output=True, text=True)

        self.assertIsNotNone(result)
        self.assertEqual(args, result.args)
        self.assertEqual('Hello\nWorld\n', result.stdout)
        self.assertEqual('', result.stderr)
        self._validate_not_failed(result)

    def test_subprocess_can_capture_the_stdout_in_binary(self):
        args = [self.script_home.format('command_stdout.sh')]
        result = subprocess.run(args, capture_output=True)

        self.assertIsNotNone(result)
        self.assertEqual(args, result.args)
        self.assertEqual(b'Hello\nWorld\n', result.stdout)
        self.assertEqual(b'', result.stderr)
        self._validate_not_failed(result)

    def test_subprocess_can_capture_the_stderr(self):
        args = [self.script_home.format('command_stderr.sh')]
        result = subprocess.run(args, capture_output=True, text=True)

        self.assertIsNotNone(result)
        self.assertEqual(args, result.args)
        self.assertEqual('Hello\n', result.stdout)
        self.assertEqual('Error\n', result.stderr)
        self._validate_not_failed(result)

    def test_subprocess_can_capture_the_stderr_from_python(self):
        args = [sys.executable, '/app/main/python/raises.py']

        result = subprocess.run(args, capture_output=True, text=True)

        self.assertIsNotNone(result)
        self.assertEqual(args, result.args)
        self.assertEqual('', result.stdout)
        self.assertIsNotNone(result.stderr)
        self.assertTrue('ValueError: stuff' in result.stderr)
        self.assertRaises(subprocess.CalledProcessError, result.check_returncode)

    def test_subprocess_when_fail_we_can_raise_an_exception(self):
        args = [self.script_home.format('command_error.sh')]

        try:
            subprocess.run(args, check=True)
            self.fail('A non 0 return code should fail with check=true')
        except subprocess.CalledProcessError:
            pass

    def _validate_not_failed(self, result):
        self.assertEqual(0, result.returncode)
        try:
            result.check_returncode()
        except subprocess.CalledProcessError:
            self.fail('Process should have finished successfully')
