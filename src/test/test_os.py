
import unittest
import os
import os.path
from pathlib import Path


class OsTests(unittest.TestCase):
    base_dir = '/test/os_tests'

    def setUp(self):
        self.assertFalse(os.path.isdir(OsTests.base_dir))
        os.mkdir(OsTests.base_dir)
        self.assertTrue(os.path.isdir(OsTests.base_dir))

    def tearDown(self):
        self.assertTrue(os.path.isdir(OsTests.base_dir))
        os.rmdir(OsTests.base_dir)
        self.assertFalse(os.path.isdir(OsTests.base_dir))

    def test_readlink_should_give_the_name_of_the_destination(self):
        dir_name = f'{OsTests.base_dir}/directory'
        destination = f'{OsTests.base_dir}/symlink'

        self._create_directory(dir_name)
        self._create_dir_symlink(dir_name, destination)

        destination_name = os.readlink(destination)
        self.assertEqual(dir_name, destination_name)

        self._destroy_directory(dir_name)
        self._destroy_symlink(destination)

    def test_readlink_when_reading_a_delete_destination_you_get_the_old_destination(self):
        dir_name = f'{OsTests.base_dir}/directory'
        destination = f'{OsTests.base_dir}/symlink'

        self._create_directory(dir_name)
        self._create_dir_symlink(dir_name, destination)

        self._destroy_directory(dir_name)

        destination_name = os.readlink(destination)
        self.assertEqual(dir_name, destination_name)

        self._destroy_symlink(destination)

    def _create_directory(self, dir_name):
        self.assertFalse(os.path.exists(dir_name))
        os.mkdir(dir_name)
        self.assertTrue(os.path.isdir(dir_name))

    def _destroy_directory(self, dir_name):
        self.assertTrue(os.path.isdir(dir_name))
        os.rmdir(dir_name)
        self.assertFalse(os.path.exists(dir_name))

    def _create_dir_symlink(self, target, destination):
        self.assertTrue(os.path.isdir(target))
        self.assertFalse(os.path.exists(destination))
        os.symlink(target, destination)
        self.assertTrue(os.path.islink(destination))
        self.assertTrue(os.path.isdir(target))

    def _destroy_symlink(self, symlink_name):
        self.assertTrue(os.path.islink(symlink_name))
        os.remove(symlink_name)
        self.assertFalse(os.path.exists(symlink_name))


class OsPathTests(unittest.TestCase):
    base_dir = '/test/os_path_tests'

    def setUp(self):
        self.assertFalse(os.path.isdir(OsPathTests.base_dir))
        os.mkdir(OsPathTests.base_dir)
        self.assertTrue(os.path.isdir(OsPathTests.base_dir))

    def tearDown(self):
        self.assertTrue(os.path.isdir(OsPathTests.base_dir))
        os.rmdir(OsPathTests.base_dir)
        self.assertFalse(os.path.isdir(OsPathTests.base_dir))

    def test_exists_is_true_for_a_directory(self):
        dir_name = f'{OsPathTests.base_dir}/directory'

        self.assertFalse(os.path.exists(dir_name))

        os.mkdir(dir_name)

        self.assertTrue(os.path.exists(dir_name))
        self.assertTrue(os.path.isdir(dir_name))
        self.assertFalse(os.path.isfile(dir_name))
        self.assertFalse(os.path.islink(dir_name))

        os.rmdir(dir_name)
        self.assertFalse(os.path.exists(dir_name))

    def test_exists_is_true_for_a_regular_file(self):
        file_name = f'{OsPathTests.base_dir}/test.txt'

        self.assertFalse(os.path.exists(file_name))

        Path(file_name).touch()

        self.assertTrue(os.path.exists(file_name))
        self.assertFalse(os.path.isdir(file_name))
        self.assertTrue(os.path.isfile(file_name))
        self.assertFalse(os.path.islink(file_name))

        os.remove(file_name)
        self.assertFalse(os.path.exists(file_name))

    def test_exists_is_true_for_a_symlink_pointing_to_a_dir(self):
        dir_name = f'{OsPathTests.base_dir}/directory'
        symlink_name = f'{OsPathTests.base_dir}/symlink'

        self.assertFalse(os.path.exists(dir_name))
        self.assertFalse(os.path.exists(symlink_name))

        os.mkdir(dir_name)
        self.assertTrue(os.path.exists(dir_name))

        os.symlink(dir_name, symlink_name)

        self.assertTrue(os.path.exists(symlink_name))
        self.assertFalse(os.path.isfile(symlink_name))
        # a symlink can be both true for symlink and directory
        # when pointing to a directory
        self.assertTrue(os.path.isdir(symlink_name))
        self.assertTrue(os.path.islink(symlink_name))

        os.remove(symlink_name)
        self.assertFalse(os.path.exists(symlink_name))
        self.assertTrue(os.path.exists(dir_name))

        os.rmdir(dir_name)
        self.assertFalse(os.path.exists(dir_name))

    def test_exists_is_true_for_a_symlink_no_longer_pointing_to_a_dir(self):
        dir_name = f'{OsPathTests.base_dir}/directory'
        symlink_name = f'{OsPathTests.base_dir}/symlink'

        self.assertFalse(os.path.exists(dir_name))
        self.assertFalse(os.path.exists(symlink_name))

        os.mkdir(dir_name)
        self.assertTrue(os.path.exists(dir_name))

        os.symlink(dir_name, symlink_name)
        self.assertTrue(os.path.exists(symlink_name))
        self.assertTrue(os.path.islink(symlink_name))

        os.rmdir(dir_name)
        self.assertFalse(os.path.exists(dir_name))

        self.assertFalse(os.path.isfile(symlink_name))
        self.assertFalse(os.path.isdir(symlink_name))

        os.remove(symlink_name)
        self.assertFalse(os.path.exists(symlink_name))

    def test_exists_is_true_for_a_symlink_pointing_to_a_file(self):
        file_name = f'{OsPathTests.base_dir}/file'
        symlink_name = f'{OsPathTests.base_dir}/symlink'

        self.assertFalse(os.path.exists(file_name))
        self.assertFalse(os.path.exists(symlink_name))

        Path(file_name).touch()
        self.assertTrue(os.path.exists(file_name))

        os.symlink(file_name, symlink_name)

        self.assertTrue(os.path.exists(symlink_name))
        self.assertFalse(os.path.isdir(symlink_name))
        # a symlink can be both true for file and symlink
        # when pointing to a file
        self.assertTrue(os.path.isfile(symlink_name))
        self.assertTrue(os.path.islink(symlink_name))

        os.remove(symlink_name)
        self.assertFalse(os.path.exists(symlink_name))
        self.assertTrue(os.path.exists(file_name))

        os.remove(file_name)
        self.assertFalse(os.path.exists(file_name))

    def test_exists_is_true_for_a_symlink_no_longer_pointing_to_a_file(self):
        file_name = f'{OsPathTests.base_dir}/file'
        symlink_name = f'{OsPathTests.base_dir}/symlink'

        self.assertFalse(os.path.exists(file_name))
        self.assertFalse(os.path.exists(symlink_name))

        Path(file_name).touch()
        self.assertTrue(os.path.exists(file_name))

        os.symlink(file_name, symlink_name)
        self.assertTrue(os.path.exists(symlink_name))
        self.assertTrue(os.path.islink(symlink_name))

        os.remove(file_name)
        self.assertFalse(os.path.exists(file_name))

        self.assertFalse(os.path.isdir(symlink_name))
        self.assertFalse(os.path.isfile(symlink_name))

        os.remove(symlink_name)
        self.assertFalse(os.path.exists(symlink_name))

