import unittest
from collections import namedtuple

class NamedTupleTest(unittest.TestCase):
    def test_named_tuple_can_have_a_name(self):
        Point = namedtuple('Point', 'x y')
        self.assertTrue(issubclass(Point, tuple))

    def test_named_tuple_has_a_descr(self):
        Point = namedtuple('Point', 'x y')
        self.assertEqual("<class 'test_namedtuple.Point'>", str(Point))
        self.assertEqual("<class 'test_namedtuple.Point'>", repr(Point))

    def test_named_tuple_let_you_access_the_params(self):
        Point = namedtuple('Point', 'x y')
        point = Point(2, 3)

        self.assertEqual(2, point.x)
        self.assertEqual(3, point.y)

    def test_named_tuple_can_have_many_with_same_name(self):
        PointXY = namedtuple('Point', 'x y')
        PointXY2 = namedtuple('Point', 'x y')
        PointAB = namedtuple('Point', 'a b')

        self.assertTrue(issubclass(PointXY, tuple))
        self.assertTrue(issubclass(PointAB, tuple))

        pointxy = PointXY(2, 2)
        pointab = PointAB(3, 3)

        self.assertEqual(pointxy.x, 2)
        self.assertEqual(pointab.a, 3)

    def test_an_existing_named_tuple_is_a_constructor(self):
        """ For lack of a better name, I call an instance of namedtuple
        a constructor, the idea here is to  validate that the nametuple
        object can be used to create several other instance."""
        PointTuple = namedtuple('Point', 'x y')
        a = PointTuple(1, 2)
        b = PointTuple(2, 3)

        self.assertTrue(isinstance(a, PointTuple))
        self.assertTrue(isinstance(b, PointTuple))

    def test_namedtuple_can_receive_an_array_with_attributes(self):
        Point = namedtuple('Point', ['x', 'y'])
        point = Point(1, 2)
        self.assertEqual(1, point.x)
        self.assertEqual(2, point.y)

    def test_namedtuple_defaults(self):
        Point = namedtuple('Point', ['x', 'y'])
        self.assertEqual(None, Point.__new__.__defaults__)

        try:
            point = Point()

        Point.__new__.__defaults__ = (0, 0)
        self.assertEqual((0, 0), Point.__new__.__defaults__)



