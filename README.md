# Description
I just wanted to have a space to run python tests. I haven't figured out the
entry point command, it's probably going to be a script with the right unittest
params, for now, since I just have one test file, I'll invoke it.

# Hmac

I was randomly looking at pickles, and how you can "sign" them when
transmiting and receiving pickle data. The documentation pointed to Hmac for
a way to validate the information receive and that's why I created the
`src/test/test_hmac.py`.

I had this issue once, one App provided a URL that calls the API that I
was supporting, and created a record in the API DB.

So:
1. User creates an "entity" identified by a UUID in App.
2. The App provided a URL with such UUID in it.
3. The customer requested the URL and ended up callingh the API I was
   maintaining.
4. The API took that UUID and created a record, assuming that the value existed
   in the App.

The issue was obvious, what if the customer changed the UUID, the user would
shoot himself or herself in the foot, but someone wanted to avoid that. So the
initial solution was to have the API call the App to see if the UUID was valid,
linked to a record for that customer.

But maybe we can just make the URL valid or invalid and I think providing
another parameter would help with that. Hmac could help with that, given an
input, in this case the UUID, a hash method and a private key you can generate
the digest, send it in the URL and then have the API take those params and
validate. The key would probably be shared through SNS since both have access
to that resource. I much rather prefer that than calling back the App.

The problem with the "call the api" method is that there's no talk around an SLA.

# Running

```
./operation.sh build
./operation.sh run
```

# Running without Docker

```
export PYTHONPATH=src/main/python
python -m unittest discover -s src/test
```

### Todo
