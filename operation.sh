#!/usr/bin/env bash

COMMAND=$1

USER=$(whoami)
IMG_NAME=python-test-${USER}:latest
NETWORK_NAME=${USER}-network

case $COMMAND in
    build)
        docker build \
            -f Dockerfile \
            --tag $IMG_NAME \
            --no-cache .
        ;;
    debug)
        docker run --rm -it \
            -v $PWD/src:/app \
            --entrypoint sh $IMG_NAME
        ;;
    run)
        PWD=$(pwd)
        docker run --rm \
            -v $PWD/src:/app \
            $IMG_NAME
        ;;
    mysql-query)
        docker run -it --rm \
            --network $NETWORK_NAME \
            mysql -hlocalhost -uroot -p
        ;;
    *)
        echo "Usage:"
        echo " operation.sh build  # build a new docker image."
        echo " operation.sh debug  # allows you to run a container."
        echo " operation.sh run    # runs the tests"
        echo " operation.sh mysql-query # logs into the mysql server."
        exit 1
        ;;
esac
