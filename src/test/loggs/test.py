from loggs import LoggBottom
import unittest


class LoggLoggBottomTest(unittest.TestCase):
    log = LoggBottom("test")

    def test_can_log_in_different_levels(self):
        self.log.debug("debug message", { "context": "test" })
        self.log.info("info message", { "context": "test" })
        self.log.warning("warn message", { "context": "test" })
        self.log.error("error message", { "context": "test" })

    def test_when_no_name_is_provided_you_get_a_default_logger(self):
        logger = LoggBottom.get_logger(None)
        self.assertIsNotNone(logger)

        logger_other = LoggBottom.get_logger(123)
        self.assertIsNotNone(logger_other)

        self.assertEqual(logger, logger_other)

    def test_loggers_are_different(self):
        logger_1 = LoggBottom.get_logger("log1")
        logger_2 = LoggBottom.get_logger("log2")
        logger_def = LoggBottom.get_logger(None)

        self.assertNotEqual(logger_1, logger_2)
        self.assertNotEqual(logger_1, logger_def)
        self.assertNotEqual(logger_2, logger_def)

    def test_same_name_different_logger(self):
        logger_a = LoggBottom.get_logger("log")
        logger_b = LoggBottom.get_logger("log")

        self.assertNotEqual(logger_a, logger_b)

    def test_an_empty_name_returns_default_logger(self):
        log_a = LoggBottom.get_logger(None)
        log_b = LoggBottom.get_logger("")
        log_c = LoggBottom.get_logger("    ")

        self.assertEqual(log_a, log_b)
        self.assertEqual(log_a, log_c)

