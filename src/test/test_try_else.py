import unittest
from unittest.mock import Mock


class TryElseTest(unittest.TestCase):

    def try_else(self) -> None:
        try:
            self.do_something()
        except RuntimeError:
            self.handle_mock()
        else:
            self.else_mock()
        finally:
            self.finally_mock()

    def setUp(self):
        self.do_something = Mock()
        self.handle_mock = Mock()
        self.finally_mock = Mock()
        self.else_mock = Mock()

    def test_normal_operation_calls_else(self):
        self.try_else()
        self.assertTrue(self.do_something.called)
        self.assertFalse(self.handle_mock.called)
        self.assertTrue(self.finally_mock.called)
        self.assertTrue(self.else_mock.called)

    def test_raise_operation_does_not_call_else(self):
        self.do_something = Mock(side_effect=RuntimeError('TryElse'))
        self.try_else()
        self.assertTrue(self.do_something.called)
        self.assertTrue(self.handle_mock.called)
        self.assertTrue(self.finally_mock.called)
        self.assertFalse(self.else_mock.called)

    def test_raise_unhandled_exception_else_not_called(self):
        self.do_something = Mock(side_effect=KeyError('Not handled'))

        try:
            self.try_else()
            self.fail('not handled')
        except KeyError as e:
            self.assertEqual('\'Not handled\'', str(e))

        self.assertTrue(self.do_something.called)
        self.assertFalse(self.handle_mock.called)
        self.assertFalse(self.else_mock.called)
        self.assertTrue(self.finally_mock.called)

    def test_handling_raises_exception_else_not_called(self):
        self.do_something = Mock(side_effect=RuntimeError('do_something'))
        self.handle_mock = Mock(side_effect=RuntimeError('handle_mock'))

        try:
            self.try_else()
            self.fail('should raise an exception')
        except RuntimeError as e:
            self.assertEqual('handle_mock', str(e))

        self.assertTrue(self.do_something.called)
        self.assertTrue(self.handle_mock.called)
        self.assertFalse(self.else_mock.called)
        self.assertTrue(self.finally_mock.called)
