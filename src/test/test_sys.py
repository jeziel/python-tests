
import unittest
import sys


class SysRefCountTests(unittest.TestCase):
    """
    https://docs.python.org/3/library/sys.html#sys.getrefcount

    basically, it'll always be, at a minimum 2, because asking for the ref count
    of an immutable value?
    """

    min_references = 2

    def test_getrefcount_undefined_var_raises_exception(self):
        try:
            # this is basically python's behavior
            self.assertRaises(NameError, sys.getrefcount, a)
            self.fail('An undeclared variable should raise an exception')
        except NameError as e:
            self.assertEqual('name \'a\' is not defined', str(e))

    def test_getrefcount_none_ref_has_a_count(self):
        none_refs = sys.getrefcount(None)
        self.assertTrue(none_refs > self.min_references, none_refs)
        self.assertTrue(sys.getrefcount(None) > self.min_references)

    def test_getrefcount_has_an_int_ref(self):
        self.assertEqual(self.min_references, sys.getrefcount(19801130))

        a = 19801130

        self.assertEqual(3, sys.getrefcount(a))
        self.assertEqual(3, sys.getrefcount(19801130))

        a = 120587952

        self.assertEqual(self.min_references, sys.getrefcount(19801130))
        self.assertEqual(3, sys.getrefcount(120587952))
        self.assertEqual(3, sys.getrefcount(a))

        del a

        self.assertEqual(self.min_references, sys.getrefcount(120587952))

    def test_getrefcount_string(self):
        self.assertEqual(self.min_references, sys.getrefcount('peter'))

        a = 'peter'

        self.assertEqual(3, sys.getrefcount('peter'))
        self.assertEqual(3, sys.getrefcount(a))

        self.assertEqual(self.min_references, sys.getrefcount('frank'))
        a = 'frank'

        self.assertEqual(self.min_references, sys.getrefcount('peter'))
        self.assertEqual(3, sys.getrefcount('frank'))

        del a

        self.assertEqual(self.min_references, sys.getrefcount('frank'))

    def test_getrefcount_del_a_variable(self):
        a = 'watts'

        self.assertEqual(3, sys.getrefcount(a))

        del a

        try:
            # this is basically python's behavior
            self.assertEqual(3, sys.getrefcount(a))
            self.fail('Referencing a deleted variable should raise an excep')
        except UnboundLocalError as e:
            self.assertEqual('local variable \'a\' referenced before assignment', str(e))

    def test_sys_executable_has_a_value(self):
        exe = sys.executable
        self.assertEqual('/usr/local/bin/python', exe)


class SysGetSwitchIntervalTests(unittest.TestCase):
    def test_getswitchinterval_has_a_value(self):
        self.assertEqual(0.005, sys.getswitchinterval())
