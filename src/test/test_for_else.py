import unittest
from unittest.mock import Mock

do_mock = Mock()
done_mock = Mock()

def for_else(break_loop: bool = False, raise_loop: bool = False) -> None:
    for i in range(5):
        if break_loop and i == 2:
            break
        if raise_loop and i == 2:
            raise RuntimeError('ForElseLoop')

        do_mock()
    else:
        done_mock()


class ForElseTest(unittest.TestCase):
    def tearDown(self):
        do_mock.reset_mock()
        done_mock.reset_mock()

    def test_when_for_completed_we_evaluate_else(self):
        for_else()
        self.assertEqual(5, do_mock.call_count)
        self.assertEqual(1, done_mock.call_count)

    def test_when_loop_breaks_else_is_not_called(self):
        for_else(True)
        self.assertEqual(2, do_mock.call_count)
        self.assertFalse(done_mock.called)

    def test_when_loop_raises_else_is_not_called(self):
        try:
            for_else(False, True)
            self.fail('should raise an exception')
        except RuntimeError as e:
            self.assertEqual('ForElseLoop', str(e))

        self.assertEqual(2, do_mock.call_count)
        self.assertFalse(done_mock.called)
