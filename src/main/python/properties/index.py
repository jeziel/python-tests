
class Something:
    def __init__(self, my_prop: int):
        """
        Uhm, so this initial _ is interpreted as "protected" notice the quotes,
        quote protected end quote, "protected". Protected means something in
        terms of visibility like only derived classes should use it?
        Or would it mean something to the developer, "you shouldn't be accessing
        this attribute it is quote protected end quote"
        """
        self._my_prop = my_prop

    @property
    def my_prop(self) -> int:
        return self._my_prop

    @my_prop.setter
    def my_prop(self, the_value: int) -> None:
        self._my_prop = the_value

    @my_prop.deleter
    def my_prop(self) -> None:
        raise RuntimeError("Unable to delete")



something = Something(None)
assert something._my_prop == None
something._my_prop = "hello"
assert something._my_prop == "hello"

another = Something("HellO")
assert another._my_prop == "HellO"

"""
Wether you assign a value to the attribute or the Property,
you'll end up with the same value. The Property method just
sets the value.
"""
my_property = Something("Hello")
assert my_property._my_prop == "Hello"
assert my_property._my_prop == my_property.my_prop
my_property._my_prop = "World"
assert my_property._my_prop == "World"
assert my_property._my_prop == my_property.my_prop

"""
Assigning a value to the Proprety updates the attribute
"""
something = Something("Hello")
something.my_prop == "Hello"
something.my_prop = 5
assert something.my_prop == 5
assert something._my_prop == something.my_prop

"""
Assigning a value to the attribute, the getter returns that value
"""
something._my_prop = 6
assert something._my_prop == 6
assert something._my_prop == something.my_prop

"""
Deleting the attribute should be handled by the attribute.
"""
something = Something("Hello")
assert hasattr(something, '_my_prop')
assert something._my_prop == "Hello"
del something._my_prop
assert hasattr(something, "_my_prop") == False

try:
    something._my_prop
    assert True == False, "The attribute does not exists."
except AttributeError as e:
    assert str(e) == "'Something' object has no attribute '_my_prop'"

"""
Just by assigning a value to an "attribute" in the instance, you'll end up with
that attribute again.
"""
something._my_prop = "World"
assert hasattr(something, '_my_prop')
assert something._my_prop == "World"

"""
What about attributes that were never part of the class? Yes, you can add
attributes on the FLY.
"""
assert hasattr(something, "never_here") == False
something.never_here = 5
assert hasattr(something, "never_here")
assert something.never_here == 5

"""
Our deleted doesn't allow deletes, when calling the Property, you still can
call the delete on an attribute.
"""
something = Something("Hello")
assert something.my_prop == "Hello"
try:
    del something.my_prop
    assert True == False, "The deleter call should have failed."
except RuntimeError as e:
    assert str(e) == "Unable to delete"

assert hasattr(something, "my_prop")
assert hasattr(something, "_my_prop")


"""
There's no relation between the name of the attribute and
the name of the Property method.
"""
class NoRelation:

    def __init__(self, hello):
        self._hello = hello

    @property
    def world(self) -> str:
        return self._hello


no_relation = NoRelation("Hello")
assert no_relation.world == "Hello"
assert no_relation._hello == no_relation.world


"""
If you don't have a setter then you won't be able to set a value.
"""
class NoSetter:
    def __init__(self, value: str):
        self._value = value

    @property
    def value(self) -> str:
        return self._value


no_setter = NoSetter("Hello")
assert no_setter.value == "Hello"
try:
    no_setter.value = "World"
    assert False, "You cannot set without a setter"
except AttributeError as e:
    assert str(e) == "can't set attribute 'value'"


"""
Do the name of the setter needs to match the method?
"""
class DifferentMethodName:
    def __init__(self, value: str):
        self._value = value
        self._value_2 = value

    @property
    def boo(self) -> str:
        """
        The name of this method defines the name of the property
        """
        return self._value

    @boo.setter
    def boo_yah_exclamation(self, value) -> None:
        """
        The decorator name needs to match the previously define Property.
        But holds no relationship.
        The setter attribute is the name of the method.
        """
        self._value_2 = value


different = DifferentMethodName("Hello")
assert different.boo == "Hello"
different.boo_yah_exclamation = "World"
assert different.boo == "Hello"


"""
Can you have a setter without a @property? No, you cannot.
"""
try:
    class NoProperty:
        def __init__(self, value):
            self._value = value

        @muff.setter
        def muff(self, value):
            self._value = value

except NameError as e:
    assert str(e) == "name 'muff' is not defined"
