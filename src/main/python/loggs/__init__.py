import logging
import json
from datetime import datetime


json_formatter = logging.Formatter('%(message)s')


class LoggBottom():
    _default_logger = None

    def __init__(self, name: str, level=logging.INFO):
        ch = logging.StreamHandler()
        ch.setFormatter(json_formatter)
        ch.setLevel(level)

        self.log = logging.getLogger(name)
        self.log.addHandler(ch)

    @classmethod
    def get_logger(cls, name: str, level=logging.INFO):
        has_name = isinstance(name, str) and len(name.strip()) != 0
        if cls._default_logger is None:
            cls._default_logger = LoggBottom("default", logging.INFO)

        return LoggBottom(name, level) if has_name else cls._default_logger

    def debug(self, message: str, params) -> None:
        self._log('debug', message, params)

    def info(self, message: str, params) -> None:
        self._log('info', message, params)

    def warning(self, message: str, params) -> None:
        self._log('warning', message, params)

    def error(self, message: str, params) -> None:
        self._log('error', message, params)

    """
    def __getattr__(self, name):
        if name in ['info', 'warning', 'error']:
            return self._log
    """

    def _log(self, level, message: str, params) -> None:
        log_function = getattr(self.log, level)
        log_function(json.dumps({
            "time": str(datetime.now()),
            "log": {
                "level": level,
                "name": self.log.name,
            },
            "message": message,
            "context": params
        }))
