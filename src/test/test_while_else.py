import unittest
from unittest.mock import Mock

done_mock = Mock(return_value=None)
do_mock = Mock(return_value=None)

def while_else(break_cycle=False, raise_cycle=False) -> None:
    total_items = 5
    i = 0
    while i < total_items:
        do_mock()
        i += 1
        if break_cycle and i == 2:
            break
        if raise_cycle and i == 2:
            raise RuntimeError("Something")
    else: # nobreak
        done_mock()


class WhileElseTest(unittest.TestCase):
    def tearDown(self):
        done_mock.reset_mock()
        do_mock.reset_mock()

    def test_when_the_loop_finished_we_get_a_call_to_else(self):
        while_else()
        self.assertEqual(5, do_mock.call_count)
        self.assertEqual(1, done_mock.call_count)

    def test_when_the_loop_breaks_we_dont_call_else(self):
        while_else(True)
        self.assertEqual(2, do_mock.call_count)
        self.assertFalse(done_mock.called)

    def test_when_the_loop_raises_we_dont_call_else(self):
        try:
            while_else(False, True)
            self.fail('should raise an exception')
        except RuntimeError as e:
            self.assertEqual("Something", str(e))

        self.assertEqual(2, do_mock.call_count)
        self.assertFalse(done_mock.called)
