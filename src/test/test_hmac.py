
import unittest
import hmac
import hashlib
import os
import base64


class HmacTests(unittest.TestCase):
    shared_key = b'secret-shared-key-goes-here'
    hash_lib = hashlib.sha256

    def test_new_using_digest(self):
        expected_digest = b'A5v38LftAtiO/gk+0bPeclwbc0l20jXrNC1tkIEaYlY=\n'
        body = self._get_file_content_binary('hmac.txt')

        digest = hmac.new(HmacTests.shared_key, body, HmacTests.hash_lib).digest()
        a = base64.encodebytes(digest)

        self.assertIsNotNone(a)
        self.assertEqual(expected_digest, a)

    def test_new_using_hexdigest(self):
        expected_digest = '039bf7f0b7ed02d88efe093ed1b3de725c1b734976d235eb342d6d90811a6256'
        body = self._get_file_content_binary('hmac.txt')

        digest_maker = hmac.new(HmacTests.shared_key, body, HmacTests.hash_lib)
        digest = digest_maker.hexdigest()

        self.assertIsNotNone(digest)
        self.assertEqual(expected_digest, digest)

    def _get_file_content_binary(self, file_name):
        dirpath = os.path.dirname(__file__)
        path = os.path.join(dirpath, f'resources/{file_name}')
        with open(path, 'rb') as f:
            body = f.read()

        self.assertIsNotNone(body, 'file not found')

        return body
