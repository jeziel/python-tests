
import unittest
import asyncio
import types


class AsyncIOTests(unittest.TestCase):
    def test_asyncio_can_determin_if_coroutine(self):
        def test_fun():
            pass

        async def test_async():
            pass

        @asyncio.coroutine
        def test_async_decorated():
            pass


        self.assertFalse(asyncio.iscoroutinefunction(test_fun))
        self.assertTrue(asyncio.iscoroutinefunction(test_async))
        self.assertTrue(asyncio.iscoroutinefunction(test_async_decorated))

    def test_asyncio_methods_return_value(self):
        async def my_function():
            return 'hello'

        result = my_function()

        self.assertIsNotNone(result)
        self.assertEqual(types.CoroutineType, type(result))

    def test_the_result_can_be_tested(self):
        async def my_function():
            return 'hello'

        def my_other_function():
            return 'hello'

        result = my_function()

        self.assertIsNotNone(result)
        self.assertTrue(asyncio.iscoroutine(result))

        result = my_other_function()

        self.assertIsNotNone(result)
        self.assertFalse(asyncio.iscoroutine(result))

    def test_asyncio_create_tasks_without_running_loop_raises_exception(self):
        async def my_function():
            return 'hello'

        self.assertRaises(RuntimeError, asyncio.create_task, my_function)
