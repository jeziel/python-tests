
def method_ellipsis() -> None:
    ...

def method_pass() -> None:
    pass

assert method_ellipsis() is None
assert method_pass() is None
assert repr(...) == 'Ellipsis'
