from collections import defaultdict


UserFriendsCount = tuple[int, int]
FriendshipCount = list[UserFriendsCount]


def average_friend_amount(friendships: dict) -> float:
    if not friendships:
        return 0

    total_items = len(friendships)
    total_relations = sum([len(v) for v in friendships.values()])
    return total_relations / total_items


def get_relations_sorted_by_amount(friendships: dict) -> FriendshipCount:
    friendship_count = [(k, len(v)) for k, v in friendships.items()]
    return sorted(friendship_count, key=lambda x: x[1], reverse=True)


def get_users_friendships(friendships: list[tuple[int, int]]) -> dict[int, list[int]]:
    user_friendships = defaultdict(list)
    for friend_1, friend_2 in friendship_pairs:
        user_friendships[friend_1].append(friend_2)
        user_friendships[friend_2].append(friend_1)

    return user_friendships


def friends_of_my_friends(user_id: int, friendships: dict[int, list[int]]) -> set[int]:
    fomf = set()
    if not friendships or user_id not in friendships:
        return fomf

    my_friends = friendships[user_id]
    for my_friend in my_friends:
        for my_ff in friendships.get(my_friends, []):
            if my_ff == user_id or my_ff in my_friends:
                continue




users = [
    { "id": 0, "name": "Hero" },
    { "id": 1, "name": "Dunn" },
    { "id": 2, "name": "Sue" },
    { "id": 3, "name": "Chi" },
    { "id": 4, "name": "Thor" },
    { "id": 5, "name": "Clive" },
    { "id": 6, "name": "Hicks" },
    { "id": 7, "name": "Devin" },
    { "id": 8, "name": "Kate" },
    { "id": 9, "name": "Klein" },
]

friendship_pairs = [
    (0,  1),
    (0,  2),
    (1,  2),
    (1,  3),
    (2,  3),
    (3,  4),
    (4,  5),
    (5,  6),
    (5,  7),
    (6,  8),
    (7,  8),
    (8,  9),
]

user_friendships = get_users_friendships(friendship_pairs)
friendship_sorted = get_relations_sorted_by_amount(user_friendships)
avg_friendships = average_friend_amount(user_friendships)
total_users = len(users)

assert len(user_friendships) == total_users
assert user_friendships[0] == [1, 2]
assert user_friendships[1] == [0, 2, 3]
assert user_friendships[2] == [0, 1, 3]
assert user_friendships[3] == [1, 2, 4]
assert user_friendships[4] == [3, 5]
assert user_friendships[5] == [4, 6, 7]
assert user_friendships[6] == [5, 8]
assert user_friendships[7] == [5, 8]
assert user_friendships[8] == [6, 7, 9]
assert user_friendships[9] == [8]
assert avg_friendships == 2.4
assert len(friendship_sorted) == total_users
assert friendship_sorted[0] == (1, 3)
assert friendship_sorted[1] == (2, 3)
assert friendship_sorted[2] == (3, 3)
assert friendship_sorted[3] == (5, 3)
assert friendship_sorted[4] == (8, 3)
assert friendship_sorted[5] == (0, 2)
assert friendship_sorted[6] == (4, 2)
assert friendship_sorted[7] == (6, 2)
assert friendship_sorted[8] == (7, 2)
assert friendship_sorted[9] == (9, 1)
