#!/usr/bin/env bash

case $1 in
    build)
        docker build -t firefox .
        ;;
    run)
        docker run -ti --rm \
           -e DISPLAY=$DISPLAY \
           -v /tmp/.X11-unix:/tmp/.X11-unix \
           firefox
        ;;
    *)
        echo "./command.sh build # builds the image"
        echo "./command.sh run # runs something in the container"

esac
